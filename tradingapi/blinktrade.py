import urllib
import urllib.request
import json
import time
import hmac, hashlib
import requests
import csv
import datetime
from indicators.priceaverage import PriceAverage
import socket


class BlinkTrade:

    def __init__(self, api_key, secret, currency='BRL', crypto_currency='BTC', environment='test'):
        self.api_key = api_key
        self.secret = secret
        self.currency = currency
        self.crypto_currency = crypto_currency
        self.environment = environment
        if self.environment == 'test':
            self.base_url = 'https://api.testnet.blinktrade.com/'
        elif self.environment == 'prod':
            self.base_url = 'https://api.blinktrade.com/'

    def ticker(self):
        url = self.base_url + 'api/v1/' + self.currency + '/ticker'
        try:
            ret = requests.get(url)
        except socket.error as err:
            print(err)
        return ret.json()

    def trades(self, start_date=None):
        url = self.base_url + 'api/v1/' + self.currency + '/trades?'
        url += 'crypto_currency='+self.crypto_currency
        if start_date is not None:
            url += '&since='+str(start_date)
        try:
            ret = requests.get(url)
        except socket.error as err:
            print(err)
        return ret.json()

    def orders(self):
        url = self.base_url + 'api/v1/' + self.currency + '/orderbook'
        try:
            ret = requests.get(url)
        except socket.error as err:
            print(err)
        return ret.json()

    def create_historical_data_file(self, start_date=None, end_date=None, filename="data.csv"):
        if end_date is None:
            end_date = time.time()
        with open(filename, 'w', newline='') as csv_file:
            line = csv.writer(csv_file, delimiter=';', quotechar='|', quoting=csv.QUOTE_MINIMAL)
            line.writerow(["tid", "timestamp", "date", "price", "amount", "side"])
            last_date = start_date
            last_tid = None
            while int(last_date) < int(end_date):
                for item in self.trades(last_date):
                    if last_tid is None or (last_tid is not None and item["tid"] != last_tid):
                        line.writerow(self.__format_data(item))
                last_date = int(item["date"])

    def get_period_information(self, start_date=None, end_date=None):
        data = []
        if end_date is None:
            end_date = time.time()
        last_date = start_date
        last_tid = None
        while int(last_date) < int(end_date):
            for item in self.trades(last_date):
                if last_tid is None or (last_tid is not None and item["tid"] != last_tid):
                    last_tid = item["tid"]
                    data.append(self.__format_data(item))
            last_date = int(item["date"])
        return data

    def __format_data(self,item):
        return [
            item["tid"],
            item["date"],
            datetime.datetime.fromtimestamp(int(item["date"])).strftime('%d-%m-%Y %H:%M:%S'),
            item["price"],
            item["amount"],
            item["side"]
        ]

    def get_chart_data(self, start_date=None, end_date=None, period=100):
        average = PriceAverage()
        last_date = start_date
        if end_date is None:
            end_date = start_date + period
            end_period = end_date
        elif end_date is not None and (start_date + period) >= end_date:
            end_period = end_date
        else:
            end_period = last_date + period

        last_tid = None
        chart = {
            'date' : None,
            'high' : 0.0,
            'low'  : 0.0,
            'open' : 0.0,
           'close' : 0.0,
             'avg' : 0.0,
          'volume' : 0.0,
           'color' : ''
        }
        data_array = []
        while int(end_period) <= int(end_date):
            while int(last_date) <= int(end_period):
                print(last_date)
                for item in self.trades(last_date):
                    average.compute(item["price"], item['amount'])
                    if last_tid is None:
                        chart['date'] = item['date']
                        chart['high'] = item['price']
                        chart['low'] = item['price']
                        chart['open'] = item['price']
                        chart['close'] = item['price']
                        chart['avg'] = average.get_price_average()
                        chart['color'] = 'green'
                    elif last_tid is not None and item["tid"] != last_tid:
                        if item["price"] > chart['high']: chart['high'] = item['price']
                        if item['price'] < chart['low'] : chart['low'] = item['price']
                        if item['price'] > chart['close']:
                            chart['color'] = 'green'
                        else:
                            chart['color'] = 'red'
                            chart['close'] = item['price']
                            chart['avg'] = average.get_price_average()
                    last_tid = item['tid']
                    last_date = item['date']
            last_tid = None
            data_array.append(chart)
            if int(last_date) < int(end_date):
                end_period = last_date + period
            elif int(last_date) > int(end_date):
                end_period = end_date + 1
            else:
                end_period = end_date
        return data_array

