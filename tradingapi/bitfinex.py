import time
import hmac, hashlib
import requests
import csv
import datetime
from indicators.priceaverage import PriceAverage
import socket


class Bitfinex:

    def __init__(self, api_key, secret, symbol="iotbtc"):
        self.api_key = api_key
        self.secret = secret
        self.symbol = symbol
        self.base_url = 'https://api.bitfinex.com/'

    def ticker(self):
        url = self.base_url + 'api/v1/' + 'pubticker/' + self.symbol
        try:
            ret = requests.get(url)
        except socket.error as err:
            print(err)
        return ret.json()

    def orders(self):
        url = self.base_url + 'api/v1/' + 'book/' + self.symbol
        try:
            ret = requests.get(url)
        except socket.error as err:
            print(err)
        return ret.json()