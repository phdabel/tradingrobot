import time
import sys, getopt, os
from tradingapi import blinktrade
import datetime
import numpy as np
from indicators.indicators import Indicators
from charts.linechart import LineChart
from scipy.signal import argrelmax, argrelmin

def main(argv):
    i = 0
    period = 50
    chart = LineChart("Bitcoin price variation",period)
    prices = []
    tids = []
    startTime = None

    try:
        opts, args = getopt.getopt(argv,"hp:c:n:s:e:",["period=","currency=","points="])
    except getopt.GetoptError:
        print('blinktrade-bot.py -p <period> -c <currency pair> -n <period of moving average>')
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-h':
            print('blinktrade-bot.py -p <period> -c <currency pair> -n <period of moving average>')
            sys.exit()
        elif opt in ("-p", "--period"):
            period = int(arg)
        elif opt in ("-c", "--currency"):
            pair = arg
        elif opt in ("-n", "--points"):
            lengthOfMA = int(arg)
        elif opt in ("-s"):
            startTime = arg
        elif opt in ("-e"):
            endTime = arg

    api_key = os.environ.get('API_KEY') or "api_key"
    api_secret = os.environ.get('API_SECRET') or "api_secret"

    conn = blinktrade.BlinkTrade(api_key,api_secret, environment="prod")
    i = 0
    lastDate = startTime
    ind = Indicators()
    totalPrices = []

    while True:

        if(lastDate is None):
            trades = conn.trades()
            lastDate = trades[-1]['date']
            lastTID = trades[-1]["tid"]
        else:
            trades = conn.trades(lastDate)
            lastDate = trades[-1]["date"]
            lastTID = trades[-1]["tid"]
            if(lastTID == trades[0]["tid"]):
                trades = trades[1:]

        for trade in trades:
            prices.append(float(trade["price"]))

            if (i == 0):
                currentValue = prices[-1]
            else:
                currentValue = ind.movingAverage(prices, period)

            tids.append(int(trade['tid']))

            chart.addXLabel("BRLBTC", i, currentValue, color='blue')
            if(len(prices) > 0):
                # local maxima
                max_locations = argrelmax(np.array(totalPrices))[0]
                # local minima
                min_locations = argrelmin(np.array(totalPrices))[0]

                if (len(max_locations) > 0):
                    chart.addXLabel("LocalMax", max_locations[-1], totalPrices[max_locations[-1]], color='green',
                                    marker='^', markersize=10)
                if (len(min_locations) > 0):
                    chart.addXLabel("LocalMin", min_locations[-1], totalPrices[min_locations[-1]], color='red',
                                    marker='v', markersize=10)

            chart.plot()
            totalPrices.append(currentValue)
            i += 1

            tids = tids[-period:]
            prices = prices[-period:]

if __name__ == "__main__":
    main(sys.argv[1:])