from bot.trading_btc_brl_bot import TradingBTCBRLBot
import os
import json

if __name__ == "__main__":

    api_key = os.environ.get('API_KEY') or "api_key"
    api_secret = os.environ.get('API_SECRET') or "api_secret"

    bot = TradingBTCBRLBot(id=0, amount_btc=1.0, amount_brl=0.0)
    print(json.dumps(bot.compute_sale_price(),indent=2))