import time
import sys, getopt, os
from tradingapi import blinktrade
from tradingapi.bitfinex import Bitfinex
import json
import csv
import datetime
import numpy as np
from indicators.indicators import Indicators
from charts.linechart import LineChart
from scipy.signal import argrelmax, argrelmin

if __name__ == "__main__":

    startTime = None
    endTime = None

    api_key = os.environ.get('API_KEY') or "api_key"
    api_secret = os.environ.get('API_SECRET') or "api_secret"

    #conn = blinktrade.BlinkTrade(api_key, api_secret, environment="prod")
    #print(json.dumps(conn.get_chart_data(start_date=1503634000, period=500, end_date=1504032782),indent=4))

    conn = Bitfinex(api_key, api_secret)
    print(conn.ticker())