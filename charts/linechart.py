import matplotlib.pyplot as plt
from matplotlib import dates

class LineChart:

    __title = None
    __period = 0
    x_labels = None
    figure = None
    __hfmt = None

    def __init__(self, title=None, period=None):
        self.__hfmt = dates.DateFormatter('%d/%m/%Y %H:%M')

        if(title is not None):
            self.__title = title
        if(period is not None):
            self.__period = -period
        self.__initialize()
        return

    def addXLabel(self, label, xValue, yValue, color="r", marker=None, markersize=None):
        if(self.x_labels == None):
            self.x_labels = {label :
                {
                "values":{"x":[xValue],"y":[yValue]},
                "figure":self.figure.add_subplot(111),
                "color":color,
                "marker":marker,
                "markersize":markersize
                }
            }
        elif (label in self.x_labels.keys()):
            self.x_labels[label]["values"]["x"].append(xValue)
            self.x_labels[label]["values"]["y"].append(yValue)
            self.x_labels[label]["marker"] = marker
            self.x_labels[label]["markersize"] = markersize
            self.x_labels[label]["color"] = color
        else:
            self.x_labels[label] = {
                "values":{"x":[xValue],"y":[yValue]},
                "figure":self.figure.add_subplot(111),
                "color":color,
                "marker":marker,
                "markersize": markersize
            }

    def pushNewValue(self, label, xValue, yValue):
        if(self.x_labels == None):
            raise Exception('Invalid label')
        elif (label not in self.x_labels.keys()):
            raise Exception('Invalid label')
        else:
            self.x_labels[label]["values"]["x"].append(xValue)
            self.x_labels[label]["values"]["y"].append(yValue)

    def __initialize(self):
        plt.ion()
        self.figure = plt.figure()

    def plot(self):
        plt.cla()
        listKeys = []
        if(self.__title is not None):
            plt.title(self.__title)

        for key in self.x_labels.keys():
            if(self.x_labels[key]["marker"] is None):
                self.x_labels[key]["figure"].plot(self.x_labels[key]["values"]["x"][self.__period:],
                                                  self.x_labels[key]["values"]["y"][self.__period:],
                                                  color=self.x_labels[key]["color"],label=key)
                #self.x_labels[key]["figure"].xaxis.set_major_formatter(self.__hfmt)
                listKeys.append(key)
            else:
                self.x_labels[key]["figure"].plot(self.x_labels[key]["values"]["x"][self.__period:],
                                                  self.x_labels[key]["values"]["y"][self.__period:],
                                                  color=self.x_labels[key]["color"],
                                                  marker=self.x_labels[key]["marker"],
                                                  markersize=self.x_labels[key]["markersize"],
                                                  linewidth=0,
                                                  label=key)
                #self.x_labels[key]["figure"].xaxis.set_major_formatter(self.__hfmt)
                listKeys.append(key)
        plt.grid(True)
        plt.legend(listKeys)
        plt.pause(0.0001)
