
class PriceAverage:

    cum_qty = None
    avg_prc = None

    def __init__(self):
        self.cum_qty = .0
        self.avg_prc = .0
        return

    def compute(self, prc, qty):
        if qty > 0:
            self.avg_prc = ((prc*qty)+(self.avg_prc*self.cum_qty))/(qty+self.cum_qty)
        return

    def get_price_average(self):
        if self.avg_prc is not None:
            return self.avg_prc
        return None

    def get_cumulative_amount(self):
        if self.cum_qty is not None:
            return self.cum_qty
        return None