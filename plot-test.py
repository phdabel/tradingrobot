import random, math
from charts.linechart import LineChart
from indicators.indicators import Indicators

priceBTC = []
MA = []

chart = LineChart()
ind = Indicators()

for i in range(200):
    if(len(priceBTC)>0):
        x = random.uniform(-9.0,9.0)
        y = priceBTC[-1] + x
        chart.pushNewValue("priceBTC",y)
        chart.pushNewValue("MA",ind.movingAverage(priceBTC,12))
        if(i > 12 and i <= 26):
            chart.pushNewValue("EMA",ind.EMA(priceBTC,12)[-1])
            chart.pushNewValue("MACD",y)
        elif(i > 26):
            chart.pushNewValue("EMA", ind.EMA(priceBTC, 12)[-1])
            chart.pushNewValue("MACD",ind.MACD(priceBTC)[0][-1])
        else:
            chart.pushNewValue("EMA", y)
            chart.pushNewValue("MACD",y)
    else:
        y = 12749.97
        chart.addXLabel("priceBTC",y,"r")
        chart.addXLabel("MA",y,"b")
        chart.addXLabel("EMA",y,"g")
        chart.addXLabel("MACD",y,"l")
    priceBTC.append(y)
    chart.plot()