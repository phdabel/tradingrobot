

class Trade:

    def __init__(self, amount, price, action, bot):
        self.amount = amount
        self.price = price
        self.action = action
        self.bot = bot

    def execute(self, created_at, status, id):
        self.created_at = created_at
        self.status = 'pending'
        self.id = id

    def complete(self,updated_at):
        self.updated_at = updated_at
        self.status = 'completed'

    def cancel(self, updated_at):
        self.updated_at = updated_at
        self.status = 'canceled'
