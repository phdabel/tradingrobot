import configparser
import random
import os
from bot.action.trade import Trade
from tradingapi.blinktrade import BlinkTrade


class TradingBTCBRLBot:

    trades = []
    sale_prices = []
    current_btc_amount = None


    def __init__(self, id, amount_btc, amount_brl, api_key='api_key', api_secret='api_secret', profit=0.001):
        self.profit = profit
        '''
        amount[0] - btc initial amount
        amount[1] - brl initial amount
        '''
        self.amount = [amount_btc, amount_brl]
        self.id = id
        self.api_key = api_key
        self.api_secret = api_secret
        self.conn = BlinkTrade(self.api_key, self.api_secret, environment="prod")
        self.__set_config__()

    def __set_config__(self):
        self.config_file = os.getcwd() + '/config/main.ini'
        self.config = configparser.ConfigParser()
        self.config.read(self.config_file)
        if 'config' not in self.config:
            raise Exception('Invalid configuration')
        if 'passive.order' not in  self.config['config']:
            raise Exception('Invalid configuration')
        if 'active.order' not in self.config['config']:
            raise Exception('Invalid configuration')
        if 'minimum.investment' not in self.config['config']:
            raise Exception('Invalid configuration')
        self.passive_fee = float(self.config['config']['passive.order'])
        self.active_fee = float(self.config['config']['active.order'])
        self.min_x = float(self.config['config']['minimum.investment'])

    def invest_value_btc(self):
        x = 0.0
        while x > self.amount[0] or x <= self.min_x:
            x = random.getrandbits(16)
            x /= 1e08
            if x <= self.min_x:
                x = self.min_x
                break
        return x

    def compute_sale_price(self):
        prices = self.conn.orders()
        return prices["asks"][:10]