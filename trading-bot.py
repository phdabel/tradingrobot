import time
import sys, getopt, os
import datetime
from tradingapi import poloniex
import numpy as np
from indicators.indicators import Indicators
from charts.linechart import LineChart
from matplotlib import dates
from scipy.signal import argrelmax, argrelmin

def main(argv):
    i = 0
    period = 20
    pair = "BTC_ETH"
    prices = []
    totalPrices = []
    currentMovingAverage = 0
    lengthOfMA = 50
    startTime = False #1491048000
    endTime = False #1491091200
    historicalData = False
    tradePlaced = False
    typeOfTrade = False
    dataDate = ""
    orderNumber = ""
    chart = LineChart("Bitcoin price variation",100)
    ind = Indicators()
    localMaxima = None
    tmpLenLocalMaxima = 0
    currentResistance = 0.018



    try:
        opts, args = getopt.getopt(argv,"hp:c:n:s:e:",["period=","currency=","points="])
    except getopt.GetoptError:
        print('trading-bot.py -p <period> -c <currency pair> -n <period of moving average>')
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-h':
            print('trading-bot.py -p <period> -c <currency pair> -n <period of moving average>')
            sys.exit()
        elif opt in ("-p", "--period"):
            if (int(arg) in [300, 900, 1800, 7200, 14400, 86400]):
                period = arg
            else:
                print("Poloniex requires periods in 300, 900, 1800, 7200, 14400, or 86400 s")
                sys.exit(2)
        elif opt in ("-c", "--currency"):
            pair = arg
        elif opt in ("-n", "--points"):
            lengthOfMA = int(arg)
        elif opt in ("-s"):
            startTime = arg
        elif opt in ("-e"):
            endTime = arg

    api_key = os.environ.get('API_KEY') or "api_key"
    api_secret = os.environ.get('API_SECRET') or "api_secret"

    conn = poloniex.poloniex(api_key,api_secret)

    if(startTime):
        historicalData = conn.api_query("returnChartData", {"currencyPair":pair, "start":startTime, "end":endTime, "period":period})

    while True:
        if(startTime and historicalData):
            nextDataPoint = historicalData.pop(0)
            lastPairPrice = nextDataPoint['weightedAverage']
            dataDate = datetime.datetime.fromtimestamp(int(nextDataPoint['date'])).strftime('%Y-%m-%d %H:%M:%S')
            timestampDate = nextDataPoint['date']/1000

        elif(startTime and not historicalData):
            exit()
        else:
            currentValues = conn.api_query("returnTicker")
            lastPairPrice = float(currentValues[pair]["last"])
            dataDate = datetime.datetime.now()
            timestampDate = dataDate.timestamp()/1000

        '''
        if (len(totalPrices) > 12):
            chart.pushNewValue("EMA", timestampDate, ind.EMA(totalPrices, 12)[-1])
        else:
            chart.addXLabel("EMA", timestampDate, lastPairPrice, color="gold")
        '''

        if(len(prices) > 0):
            currentMovingAverage = sum(prices) / float(len(prices))
            previousPrice = prices[-1]
            if (not tradePlaced):
                if ((lastPairPrice > currentMovingAverage) and (lastPairPrice < previousPrice)):
                    print("SELL ORDER")
                    orderNumber = conn.sell(pair,lastPairPrice,.01)
                    tradePlaced = True
                    typeOfTrade = "short"
                elif ((lastPairPrice < currentMovingAverage) and (lastPairPrice > previousPrice)):
                    print("BUY ORDER")
                    orderNumber = conn.buy(pair,lastPairPrice,.01)
                    tradePlaced = True
                    typeOfTrade = "long"
            elif (typeOfTrade == 'short'):
                if(lastPairPrice < currentMovingAverage):
                    print("EXIT TRADE")
                    conn.cancel(pair,orderNumber)
                    tradePlaced = False
                    typeOfTrade = False
            elif (typeOfTrade == 'long'):
                if(lastPairPrice > currentMovingAverage):
                    print("EXIT TRADE")
                    conn.cancel(pair,orderNumber)
                    tradePlaced = False
                    typeOfTrade = False
            chart.pushNewValue("priceBTC", i, lastPairPrice)
            chart.pushNewValue("MA", i, ind.movingAverage(prices, lengthOfMA))

            # local maxima
            max_locations = argrelmax(np.array(totalPrices))[0]
            # local minima
            min_locations = argrelmin(np.array(totalPrices))[0]
            if(len(max_locations) > 0):
                chart.addXLabel("LocalMax", max_locations[-1], totalPrices[max_locations[-1]], color='green', marker='^', markersize=10)
            if(len(min_locations) > 0):
                chart.addXLabel("LocalMin", min_locations[-1], totalPrices[min_locations[-1]], color='red', marker='v', markersize=10)

        else:
            previousPrice = 0
            chart.addXLabel("priceBTC", i, lastPairPrice, color='black')
            chart.addXLabel("MA", i, lastPairPrice, color='blue')

        print("%s Period: %ss %s: %s Moving Average: %s" % (dataDate, str(period), pair, lastPairPrice, str(currentMovingAverage)))
        chart.plot()
        prices.append(float(lastPairPrice))
        totalPrices.append(float(lastPairPrice))
        prices = prices[-lengthOfMA:]

        if (not startTime):
            time.sleep(int(period))
        i += 1

if __name__ == "__main__":
    main(sys.argv[1:])